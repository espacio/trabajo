module trabajo

go 1.13

require (
	github.com/Nerzal/gocloak v1.0.0
	github.com/Nerzal/gocloak/v4 v4.5.0
	github.com/go-resty/resty/v2 v2.1.0
	k8s.io/apimachinery v0.17.0
	k8s.io/client-go v0.17.0
)
