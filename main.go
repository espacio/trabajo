package main

import (
    "fmt"
    "os"
    "log"
    "net/http"

    "k8s.io/client-go/kubernetes"
    "k8s.io/client-go/rest"

    "github.com/Nerzal/gocloak/v4"
)

type k8s struct {
    clientset kubernetes.Interface
}

func newK8s() (*k8s, error) {
    // create in-cluster config
    config, err := rest.InClusterConfig()
    if err != nil {
        return nil, err
    }

    // create clientset
    client := k8s{}
    client.clientset, err = kubernetes.NewForConfig(config)
    if err != nil {
        return nil, err
    }

    return &client, nil
}

func (o *k8s) getVersion() (string, error) {
    version, err := o.clientset.Discovery().ServerVersion()
    if err != nil {
        return "", err
    }
    return fmt.Sprintf("%s", version), nil
}

func GetOrCreateRealm(client gocloak.GoCloak, token string, name string) (*gocloak.RealmRepresentation, error) {
    realm, err := client.GetRealm(token, name)

    if err != nil {
        // realm doesn't exist, create it
        enabled := true
        newRealm := gocloak.RealmRepresentation {
            ID: &name,
            DisplayName: &name,
            Enabled: &enabled,
        }
        _, err = client.CreateRealm(token, newRealm)
        
        if err != nil {
            return nil, err
        }

        return &newRealm, nil
    }

    return realm, nil
}

func ApiV1Server(w http.ResponseWriter, r *http.Request) {
    client := gocloak.NewClient(os.Getenv("KEYCLOAK_URL"))
	token, err := client.LoginAdmin(os.Getenv("KEYCLOAK_USERNAME"), os.Getenv("KEYCLOAK_PASSWORD"), "master")
	if err != nil {
		panic("Something wrong with the credentials or url")
	}

    _, err = client.GetRealm(token.AccessToken, "master")

    if err != nil {
        panic(err.Error())
    }

    fmt.Fprintf(w, "Hello, %s!", "World")
}

func main() {
    http.HandleFunc("/api/v1/", ApiV1Server)
    log.Println("Starting webserver")
    log.Fatal(http.ListenAndServe(":8080", nil))
    //k8s, err := newK8s()
    //if err != nil {
        //fmt.Println(err)
        //return
    //}
    //v, err := k8s.getVersion()
    //if err != nil {
        //fmt.Println(err)
        //return
    //}

    //fmt.Println(v)
}
